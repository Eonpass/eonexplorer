# MIT License - Eonpass 2020

from __future__ import print_function
from simplejson import JSONEncoder
from flask import Flask, jsonify, request
from werkzeug.middleware.proxy_fix import ProxyFix

import eonexplorer.utxo as Utxo
import eonexplorer.block as Block
import eonexplorer.tx as Tx
import eonexplorer.config as config
import eonexplorer.asset as Asset

app = Flask(__name__)
app.json_encoder = JSONEncoder #paramount to decode Decimals in json
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)

@app.route('/')
def index():
    return 'Welcome to Eonpass Explorer for test environments!'

#GET all UTXOs from the connected wallet
@app.route('/utxos', methods=['GET'])
def utxos():
    if request.method == 'GET':
        return jsonify(Utxo.getAll())

#GET a new  UTXO by spending the highest value UTXO at the moment
@app.route('/utxo', methods=['PUT'])
#expects a payload structured like { value : n }, where n is the value you want in the new UTXO
def utxo():
    headers = request.headers
    auth = headers.get("Api-Key")
    if(auth!=config.SECRET_KEY):
        return jsonify({"message": "ERROR: Unauthorized"}), 401
    if request.method == 'PUT':
        payload = request.json
        return jsonify(Utxo.put(payload))

#GET txid details
@app.route('/tx/<txid>', methods=['GET'])
#<txid> is the txid of the transaction you want to inspect
#accepts also a querystring:
#blinded=true/false, which defaults to false
#e.g. /tx/<txid>?blinded=true
def tx(txid):
    blinded = request.args.get('blinded', default = 'false', type = str) #bool is not working properly for now
    if request.method == 'GET':
        if blinded=='true':
            return jsonify(Tx.get_blinded(txid))
        else:
            return jsonify(Tx.get(txid))

#GET a new block
@app.route('/block', methods=['PUT'])
#creates 1 new block
def block():
    headers = request.headers
    auth = headers.get("Api-Key")
    if(auth!=config.SECRET_KEY):
        return jsonify({"message": "ERROR: Unauthorized"}), 401
    if request.method == 'PUT':
        payload = request.json
        return jsonify(Block.put(payload))

#GET block deatils
@app.route('/block/<blockhash>', methods=['GET'])
#<blockhash> is the blockhash of the block you want to inspect
def block_details(blockhash):
    if request.method == 'GET':
        return jsonify(Block.get(blockhash))

#GET all Assets from the connected wallet
@app.route('/assets', methods=['GET'])
def assets():
    if request.method == 'GET':
        return jsonify(Asset.get_all())

host = config.EXPLORER_HOST
app.run(debug=True, host=host) #runs on default port 5000
