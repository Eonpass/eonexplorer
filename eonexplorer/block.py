# MIT License - Eonpass 2020

# Methods for Block
import subprocess
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonexplorer.config as config
import os


address = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT)).getnewaddress();

def put(payload):
    try:
        blocks = "1"
        if(payload and payload['blocks']): blocks = str(payload['blocks'])
        #python 3.6 stdout=PIPE, stderr=PIPE
        #on EC2 multiple args don't work, probably it is some convertion to str messing, it works as a single command string though
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        if os.name == 'nt':
            result = subprocess.run([config.CLI_PATH, "-datadir="+ config.DATADIR_PATH, "generatetoaddress",blocks,address], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
            result = result.stdout
        else:
            #dubprocess.PIPE is deprecated in some environments
            result = subprocess.run([config.CLI_PATH+" -datadir="+config.DATADIR_PATH+" generatetoaddress "+blocks+" "+address], stdout=None, stderr=None, universal_newlines=True, shell=True)
            result = "new block created" #EC2 with linux cannot use stdout and stderr
        return result

    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500

def get(blockhash):
    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))
        block=rpc_connection.getblock(blockhash)
        return  block
    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500
