
# Eonexplorer

Compact elements explorer to use for testing and unit test execution.

## Installation

Eonexplorer is a Flask app which needs an active elements node on the same machine. This procedure is tested with python3 pip3 on AWS EC2 ubuntu machine.

### For Linux and Mac

This is the first installation process after cloning the project:
```
cd eonexplorer
pip install virtualenvwrapper
mkvirtualenv eonexplorer
workon eonexplorer
pip install -r requirements.txt
```

`virtualenwrapper` may require to work properly:
```
echo "source /your/path/to/bin/virtualenvwrapper.sh" >> ~/.profile
echo "source virtualenvwrapper.sh &> /dev/null" >> ~/.bashrc
```
### For windows instead

```
cd eonexplorer
pip install virtualenvwrapper-win
python -m virtualenv .
.\scripts\activate
pip install -r requirements.txt
```

### Config

Before running the app you have to create the `config.py` which contains the connections parameter to the node and other constants. The file must be in the `eonexplorer` folder, so that the structure will be as follows:

```
.
+-- app.py
+-- eonexplorer
    +-- block.py
    +-- config.py
    +-- tx.py
    +-- utxo.py

```

`config.py` file:

```
#Connection parameters to the elements node:
RPC_PORT = 9999
RPC_USER = 'youruser'
RPC_PASSWORD = 'yourpassword'
RPC_HOST = '127.0.0.1'

#Flask parameters, e.g. 0.0.0.0 for EC2, 127.0.0.1 for local dev
EXPLORER_HOST = '127.0.0.1'

#elements-cli absolute path and datadir
CLI_PATH = '/home/yourpath/elements-0.18.1.3/bin/elements-cli'
DATADIR_PATH = '/home/yourpath/elements-0.18.1.3/elementsdir'
BASE_ADDRS = 'an address generated on your test node'
#DATADIR_PATH must be absolute, cannot use ~ on EC2

# Api-key-secret for generating new blocks, note that this alone without SSL won't do anything
SECRET_KEY = 'xxxxxxxxxx'
```


## Usage

Flask is not a web server so you can run it for the testing purposes without Gunicorn or other front ends although it would be best to have it run behind a web server. In particular concurrent requests will make flask crash immediately.

```
workon eonexplorer
python app.py
```

And now you can explore the resources on `http://server_domain_or_IP:5000`

* **Tx**

  `GET` | `/tx/:txid?blinded=[true|false]`

  return details of the transaction

  * `txid` is the id for the txid you want to inspect
  * `blinded` is an optional parameter that defaults to false, returns the blinded (or not) version of the tx

  e.g.
  ```
  $ curl http://localhost:5000/tx/yourtxid?blinded=true
  ```

* **Block**

  `GET` | `/block/:blockhash`

  return details of the block
  * `:blockhash` is the hash for the block you want to inspect

  `PUT` | `/block`

  request the creationg of a new block, this is used to confirm transactions in unit tests

  * `request body` is optional and should be of the form `{"blocks":n}`, where n is an integer declaring how many blocks should be created and defaults to 1
  * `request header` must carry the custom header `Api-Key`, failing to do so returns unauthorised

  e.g.
  ```
  $ curl -X PUT -H "Api-Key: xxxxxxxx" -d '{"blocks":1}' http://localhost:5000/block
  ```

* **Utxos**

  `GET` | `/utxos`

  get the list of utxos from the node

  `PUT` | `/utxo`

  create a new utxo by spending the highest available output

  * `request body` is required and must be of the form `{"value":d}`, where d is a decimal declaring how much should be put in  the new utxo
  * `request header` must carry the custom header `Api-Key`, failing to do so returns unauthorised

  e.g.
  ```
  $ curl -X PUT -H "Api-Key: xxxxxxxx" -d '{"value":0.1}' http://localhost:5000/utxo
  ```


### Web Service Gateway Interface

To add gunicorn and ngnix

```
suto apt-install ngnix
pip install gunicorn
```

then you need a wsgi.py entrypoint, something like this:

```
from eonexplorer import app

if __name__ == "__main__":
    app.run()
```

..wip


### Notes

You can show this explorer to the public, although it is not advised to, the Api-Key is just a basic protection which anyway needs SSL in order not to be leaked. Never expose to the public the machine you use to run your unit tests for deployement in production.


## License

This repository is distributed under the terms of the MIT 2.0.
